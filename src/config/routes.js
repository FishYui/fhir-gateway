const express = require("express");
const router = express.Router();

const gate_way__controller = require("../api/controllers/gate_way");

router.get("/:resourceType/:id?", gate_way__controller.get);
router.post("/:resourceType", gate_way__controller.post);
router.put("/:resourceType/:id", gate_way__controller.put);
router.delete("/:resourceType/:id", gate_way__controller.delete);

module.exports = router;