const url = require("url");
const FHIR_HTTP = require("../helpers/HTTP2020");
const Fetch = require("../helpers/HTTPFetch");
const getTime = function (time) {

  const _time = time.split("-");
  const date = new Date(_time[0], _time[1] - 1, _time[2]);

  return date.getTime() / 1000;
};
const dateSynonyms = ["date", "effective-time"];

module.exports = {

  get: async function (req, res) {

    const { params, query, jwtPayload } = req;                            //取得request的param, query, jwtPayload
    const scopeGET = jwtPayload.scope.find(s => s.method == "GET").url;   //取得scope get類
    if (!params.id) delete params.id;  //如果request param裡面沒有id則刪除

    let checkScope = true;  //判斷scope和request檢查結果
    for (let scopeUrl of scopeGET) {  //遍歷scope get url

      checkScope = true;

      const _url = url.parse(scopeUrl);  //使用URL類將scopeUrl拆解
      const scopeUrlQuery = new URLSearchParams(_url.query); //將scope url的query字串拆解、分類

      //驗證Url.params，scope url的要和request url的一樣 => 驗證resourceType and resourceId是否一樣
      for (let key of Object.keys(params)) {
        if (_url.pathname.indexOf(params[key]) == -1) {
          checkScope = false;
          break;
        }
      }
      if (!checkScope) continue;



      //驗證Url.querys，scope url的要和request url的一樣 
      for (let [key, value] of scopeUrlQuery.entries()) {

        if (!query[key]) {  //先判斷scope有的參數request是否也有，沒有就判斷沒權限
          checkScope = false;
          break;
        }

        let requestDateRange = { little: 0, big: 9999999999 };    //設置request的時間段
        //如果scope有date的話獨立驗證
        if (dateSynonyms.indexOf(key) != -1) {

          if (!Array.isArray(query[key])) query[key] = [query[key]];

          //取得request的時間範圍
          for (let date of query[key]) {

            const time = getTime(date.substr(2));     //取得時間段
            const prefixe = date.substr(0, 2);        //取得前綴

            if (prefixe == "eq") {              //如果前綴是eq(=)，將時間段大小設一樣後直接退出
              requestDateRange.little = time;
              requestDateRange.big = time;
              break;
            }

            switch (prefixe) {
              case "gt":  //前綴是 >
                if (time + 1 > requestDateRange.little) requestDateRange.little = time + 1;
                break;
              case "ge":  //前綴是 >=
                if (time > requestDateRange.little) requestDateRange.little = time;
                break;
              case "lt":  //前綴是 <
                if (time - 1 < requestDateRange.big) requestDateRange.big = time - 1;
                break;
              case "le":  //前綴是 <=
                if (time < requestDateRange.big) requestDateRange.big = time;
                break;
            }
          }

          //取得scope的時間範圍並與request的時間範圍做比較
          for (let date of scopeUrlQuery.getAll(key)) {

            const time = getTime(date.substr(2));     //取得時間段
            const prefixe = date.substr(0, 2);        //取得前綴

            switch (prefixe) {
              case "eq":
                if (requestDateRange.little != time && requestDateRange.big != time) checkScope = false;
                break;
              case "gt":
                if (time >= requestDateRange.little) checkScope = false;
                break;
              case "ge":
                if (time > requestDateRange.little) checkScope = false;
                break;
              case "lt":
                if (time <= requestDateRange.big) checkScope = false;
                break;
              case "le":
                if (time < requestDateRange.big) checkScope = false;
                break;
            }
            if (!checkScope) break;
          }
        }
        else if (query[key] != value) {  //檢查剩餘的參數
          checkScope = false;
          break;
        }
      }
      if (!checkScope) continue;

      try {

        const result = await Fetch(scopeUrl, {});
        return res.status(200).json(result);

      } catch (e) {
        if (e.fhirError) {
          return res.status(e.status).json({ message: e.response.statusText });
        } else {
          return res.status(500).json({ message: "server error." });
        }
      }
    }

    return res.status(401).json({ message: "No Authority." });
  },

  post: async function (req, res) {

    const replaceChars = { "eq": "=\"", "gt": ">\"", "ge": ">=\"", "lt": "<\"", "le": "<=\"" };
    const { body, params, jwtPayload } = req;       //取得request的body, param, jwtPayload
    const scopePOST = jwtPayload.scope.find(s => s.method == "POST").url;   //取得scope get類

    let checkScope = true;  //判斷scope和request檢查結果
    for (let scopeUrl of scopePOST) {  //遍歷scope get url

      checkScope = true;

      const _url = url.parse(scopeUrl);  //使用URL類將scopeUrl拆解
      const scopeUrlQuery = new URLSearchParams(_url.query); //將scope url的query字串拆解、分類

      if (params.resourceType != body.resourceType || _url.pathname.indexOf(params.resourceType) == -1) {
        checkScope = false;
        continue;
      }

      //驗證scope query限制的參數，request body是否有符合 : 根據resourceType不同有不同的處理方式
      switch (params.resourceType) {
        case "MedicationRequest":
          for (let [key, value] of scopeUrlQuery.entries()) {
            switch (key) {
              case "patient":
                if (body.subject.reference != `Patient/${value}`) checkScope = false;
                break;
              case "date":
                const scopeTimeString = value.replace(/eq|gt|ge|lt|le/, c => replaceChars[c]) + "\"";     //取得時間段
                const bodyTimeString = "\"" + body.dosageInstruction[0].timing.event[0] + "\"";
                const checkTime = eval(bodyTimeString + scopeTimeString);
                if (!checkTime) checkScope = false;
                break;
              case "requester":
                if (body.requester.reference != `Practitioner/${value}`) checkScope = false;
                break;
            }
            if (!checkScope) break;
          }
          break;
        case "MedicationAdministration":
          for (let [key, value] of scopeUrlQuery.entries()) {
            switch (key) {
              case "effective-time":
                const scopeTimeString = value.replace(/eq|gt|ge|lt|le/, c => replaceChars[c]) + "\"";     //取得時間段
                const bodyTimeString = "\"" + body.effectiveDateTime + "\"";
                const checkTime = eval(bodyTimeString + scopeTimeString);
                if (!checkTime) checkScope = false;
                break;
              case "subject":
                if (body.subject.reference != `Patient/${value}`) checkScope = false;
                break;
            }
            if (!checkScope) break;
          }
          break;
        case "Observation":
          for (let [key, value] of scopeUrlQuery.entries()) {
            switch (key) {
              case "date":
                const scopeTimeString = value.replace(/eq|gt|ge|lt|le/, c => replaceChars[c]) + "\"";     //取得時間段
                const bodyTimeString = "\"" + body.effectiveDateTime + "\"";
                const checkTime = eval(bodyTimeString + scopeTimeString);
                if (!checkTime) checkScope = false;
                break;
              case "patient":
                if (body.subject.reference != `Patient/${value}`) checkScope = false;
                break;
            }
            if (!checkScope) break;
          }
          break;
      }
      if (!checkScope) continue;

      return res.status(200).json({ message: "Post Success." });
    }

    return res.status(401).json({ message: "No Authority." });
  },

  put: async function (req, res) {

    const replaceChars = { "eq": "=\"", "gt": ">\"", "ge": ">=\"", "lt": "<\"", "le": "<=\"" };
    const { body, params, jwtPayload } = req;       //取得request的body, param, jwtPayload
    const scopePUT = jwtPayload.scope.find(s => s.method == "PUT").url;   //取得scope put類

    if(body.resourceType != params.resourceType || body.id != params.id) {  //檢查body的resourceType和Id是否和URL指定的一樣
      return res.status(403).json({ message: "resourceType or Id Error in Body and Url." });
    }

    let checkScope = true;  //判斷scope和request檢查結果
    for (let scopeUrl of scopePUT) {  //遍歷scope get url

      checkScope = true;

      const _url = url.parse(scopeUrl);  //使用URL類將scopeUrl拆解
      const scopeUrlQuery = new URLSearchParams(_url.query); //將scope url的query字串拆解、分類

      if (_url.pathname.indexOf(params.resourceType) == -1) {  //檢查request的resourceType是否和scope的一樣
        checkScope = false;
        continue;
      }

      //驗證scope query限制的參數，request body是否有符合 : 根據resourceType不同有不同的處理方式
      switch (params.resourceType) {
        case "MedicationRequest":
          for (let [key, value] of scopeUrlQuery.entries()) {
            switch (key) {
              case "patient":
                if (body.subject.reference != `Patient/${value}`) checkScope = false;
                break;
              case "date":
                const scopeTimeString = value.replace(/eq|gt|ge|lt|le/, c => replaceChars[c]) + "\"";     //取得時間段
                const bodyTimeString = "\"" + body.dosageInstruction[0].timing.event[0] + "\"";
                const checkTime = eval(bodyTimeString + scopeTimeString);
                if (!checkTime) checkScope = false;
                break;
              case "requester":
                if (body.requester.reference != `Practitioner/${value}`) checkScope = false;
                break;
            }
            if (!checkScope) break;
          }
          break;
        case "MedicationAdministration":
          for (let [key, value] of scopeUrlQuery.entries()) {
            switch (key) {
              case "effective-time":
                const scopeTimeString = value.replace(/eq|gt|ge|lt|le/, c => replaceChars[c]) + "\"";     //取得時間段
                const bodyTimeString = "\"" + body.effectiveDateTime + "\"";
                const checkTime = eval(bodyTimeString + scopeTimeString);
                if (!checkTime) checkScope = false;
                break;
              case "subject":
                if (body.subject.reference != `Patient/${value}`) checkScope = false;
                break;
            }
            if (!checkScope) break;
          }
          break;
        case "Observation":
          for (let [key, value] of scopeUrlQuery.entries()) {
            switch (key) {
              case "date":
                const scopeTimeString = value.replace(/eq|gt|ge|lt|le/, c => replaceChars[c]) + "\"";     //取得時間段
                const bodyTimeString = "\"" + body.effectiveDateTime + "\"";
                const checkTime = eval(bodyTimeString + scopeTimeString);
                if (!checkTime) checkScope = false;
                break;
              case "patient":
                if (body.subject.reference != `Patient/${value}`) checkScope = false;
                break;
            }
            if (!checkScope) break;
          }
          break;
      }
      if (!checkScope) continue;

      return res.status(200).json({ message: "Put Success." });
    }

    return res.status(401).json({ message: "No Authority." });
  },

  delete: async function (req, res) {

    const replaceChars = { "eq": "=\"", "gt": ">\"", "ge": ">=\"", "lt": "<\"", "le": "<=\"" };
    const { params, jwtPayload } = req;       //取得request的param, jwtPayload
    const scopeDELETE = jwtPayload.scope.find(s => s.method == "DELETE").url;   //取得scope delete類

    const requestData = await (async function (url) {
      try {

        const result = await Fetch(url, {});
        return result;

      } catch (e) {
        if (e.fhirError) {
          res.status(e.status).json({ message: e.response.statusText });
        } else {
          res.status(500).json({ message: "server error." });
        }
        return false;
      }
    })(process.env.FHIR_SERVER + req._parsedUrl.path);
    if (!requestData) return;


    let checkScope = true;  //判斷scope和request檢查結果
    for (let scopeUrl of scopeDELETE) {  //遍歷scope get url

      checkScope = true;

      const _url = url.parse(scopeUrl);  //使用URL類將scopeUrl拆解
      const scopeUrlQuery = new URLSearchParams(_url.query); //將scope url的query字串拆解、分類

      if (params.resourceType != requestData.resourceType || _url.pathname.indexOf(params.resourceType) == -1) {
        checkScope = false;
        continue;
      }

      //驗證scope query限制的參數，request body是否有符合 : 根據resourceType不同有不同的處理方式
      switch (params.resourceType) {
        case "MedicationRequest":
          for (let [key, value] of scopeUrlQuery.entries()) {
            switch (key) {
              case "patient":
                if (requestData.subject.reference != `Patient/${value}`) checkScope = false;
                break;
              case "date":
                const scopeTimeString = value.replace(/eq|gt|ge|lt|le/, c => replaceChars[c]) + "\"";     //取得時間段
                const bodyTimeString = "\"" + requestData.dosageInstruction[0].timing.event[0] + "\"";
                const checkTime = eval(bodyTimeString + scopeTimeString);
                if (!checkTime) checkScope = false;
                break;
              case "requester":
                if (requestData.requester.reference != `Practitioner/${value}`) checkScope = false;
                break;
            }
            if (!checkScope) break;
          }
          break;
        case "MedicationAdministration":
          for (let [key, value] of scopeUrlQuery.entries()) {
            switch (key) {
              case "effective-time":
                const scopeTimeString = value.replace(/eq|gt|ge|lt|le/, c => replaceChars[c]) + "\"";     //取得時間段
                const bodyTimeString = "\"" + body.effectiveDateTime + "\"";
                const checkTime = eval(bodyTimeString + scopeTimeString);
                if (!checkTime) checkScope = false;
                break;
              case "subject":
                if (requestData.subject.reference != `Patient/${value}`) checkScope = false;
                break;
            }
            if (!checkScope) break;
          }
          break;
        case "Observation":
          for (let [key, value] of scopeUrlQuery.entries()) {
            switch (key) {
              case "date":
                const scopeTimeString = value.replace(/eq|gt|ge|lt|le/, c => replaceChars[c]) + "\"";     //取得時間段
                const bodyTimeString = "\"" + body.effectiveDateTime + "\"";
                const checkTime = eval(bodyTimeString + scopeTimeString);
                if (!checkTime) checkScope = false;
                break;
              case "patient":
                if (requestData.subject.reference != `Patient/${value}`) checkScope = false;
                break;
            }
            if (!checkScope) break;
          }
          break;
      }
      if (!checkScope) continue;

      return res.status(200).json({ message: "Delete Success." });
    }

    return res.status(401).json({ message: "No Authority." });
  }
}