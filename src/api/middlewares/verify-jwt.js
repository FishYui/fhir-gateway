const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {

  const token = req.headers.authorization;

  try {

    const payload = await jwt.verify(token, process.env.PORTAL_PUBLIC_KEY, { algorithm: 'RS256' });
    req.jwtPayload = payload;
    next();

  } catch (err) {

    res.status(401).json({ message: err.name });

  }
}