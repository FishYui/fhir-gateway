const fetch = require('node-fetch');

module.exports = async (url, option) => {

    const opt = option || {};

    const response = await fetch(url, opt);

    if (response.status >= 200 && response.status < 300) {
        return await response.json();
    } else {

        const error = new Error(response.statusText);

        error.response = response;
        error.status = response.status;
        error.fhirError = true;

        throw error;
    }

}