const express = require("express");
const fs = require("fs");
const routes = require('./config/routes');
const auth = require("./api/middlewares/verify-jwt");
const app = express();

if (process.env.NODE_ENV === "dev")
  require('dotenv').config({ path: "./config/env/dev.env" });
else if (process.env.NODE_ENV === "prod")
  require('dotenv').config({ path: "./config/env/prod.env" });


fs.readFile("./config/serverPtx/pubkey.key", function(error, data) {
  if(error) {
    console.debug(`讀取檔案失敗，錯誤訊息：$${error}`);
    return;
  }
  process.env.PORTAL_PUBLIC_KEY = data;
});


app.use(express.json());
app.use("/GateWay", auth, routes);


app.listen(process.env.PORT, function (req, res) {
  console.log(`System running on PORT:${process.env.PORT}`);
});